var Popular = (function() {
	
	var popularDiv = document.getElementById("popular"), popular2Div = document.getElementById("popular2"), trend = 0;
	
	function loadPopular() {
		console.log("masuk");
		if(window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.open("GET","php/GetPopular.php",false);
		xmlhttp.send();
		
		console.log(xmlhttp.responseText);
		
		data = JSON.parse(xmlhttp.responseText);
		
		if(data.status == "success") {
			for(var i=0; i < data.artikel.length; i++) {
				addPopular(data.artikel[i]);
			}
		}
	}
	
	function generateTrendItemHTML(popularData) {
		var stringhtml = "";
		stringhtml += "<div id='artikel-"+ popularData.ID_ARTIKEL +"' class='trend-item'>";
		stringhtml += "<h3>"+ popularData.JUDUL +"</h3>";
		stringhtml += "<a href='"+popularData.URL+"'>"+popularData.URL.substr(0,45)+"...</a>";
		stringhtml += "<div class='pull-right'><small>"+moment(popularData.TGL_TERBIT,"YYYY-MM-DD hh:mm:ss").fromNow()+"</small></div>"
		stringhtml += "<p>"+ popularData.FULL_TEXT.substr(0,75) +"<a href='"+popularData.URL+"'> read more ...</a></p>";
		stringhtml += "</div>";
		
		return stringhtml;
	}
	
	function addPopular(popularData) {
		if(document.getElementById("artikel-"+popularData.ID_ARTIKEL) == null) {
			if(trend > 2) {
				popular2Div.innerHTML = popular2Div.innerHTML + generateTrendItemHTML(popularData);
			} else {
				popularDiv.innerHTML = popularDiv.innerHTML + generateTrendItemHTML(popularData);
			}
			trend += 1;
		}
	}
	
	return { loadPopular : loadPopular };
})();