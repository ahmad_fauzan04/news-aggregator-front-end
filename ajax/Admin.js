var Admin = (function() {
	var odd = true;
	var firtsTime = true;
	var table = null;

	function startLoad() {
		loadData();
		setTimeout(startLoad, 3000);
	}

	function loadData() {
		if(window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				//console.log("Result : " + xmlhttp.responseText);
				var text = xmlhttp.responseText.replace(/\n/g,"<br/>");
				//console.log(text);
				data = JSON.parse(text);
				
				if(data.status == "success") {
					// Generate Show data
					if(firtsTime) {
						showConfig(data.config);
						firtsTime = false;
					}
					showStatus(data.status_crawler);
					showLog(data.log);
				} else {
				
				}
			}
		  }
		xmlhttp.open("GET","php/GetAdminData.php",true);
		xmlhttp.send();
	}

	function start() {
		if(window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				console.log("Result : " + xmlhttp.responseText);
				var text = xmlhttp.responseText;
				console.log(text);
				data = JSON.parse(text);
				
				if(data.status == "success") {
					$('#status-running').html("");
					firtsTime = true;
					loadData();
					$('#start-button').removeAttr('disabled');
		 			$('#stop-button').removeAttr('disabled');
				} else {
					
				}
			}
		  }
		xmlhttp.open("POST","php/Config.php",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("status=1");
		$('#status-running').html("Starting Crawler ...");
		 $('#start-button').attr('disabled','disabled');
		 $('#stop-button').attr('disabled','disabled');
	}

	function stop() {
		if(window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				console.log("Result : " + xmlhttp.responseText);
				var text = xmlhttp.responseText;
				console.log(text);
				data = JSON.parse(text);
				
				if(data.status == "success") {
					$('#status-running').html("");
					firtsTime = true;
					loadData();
					$('#start-button').removeAttr('disabled');
		 			$('#stop-button').removeAttr('disabled');
				} else {
					
				}
			}
		  }
		xmlhttp.open("POST","php/Config.php",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("status=0");
		$('#status-running').html("Stop Crawler ...");
		$('#start-button').attr('disabled','disabled');
		$('#stop-button').attr('disabled','disabled');
	}

	function showConfig(config) {

		if(config.status == 1) {
			$('#status-crawler').html("Started");
			$('#status-crawler').removeClass("bg-danger");
			$('#status-crawler').addClass("bg-success");
		} else {
			$('#status-crawler').html("Stoped");
			$('#status-crawler').removeClass("bg-success");
			$('#status-crawler').addClass("bg-danger");
		}

		$('[name="rss_time"]').val(config.durasi_parser);
		$('[name="cluster_time"]').val(config.durasi_cluster);
	}

	function showStatus(status) {
		//console.log(status.news_count);
		$('#news_count').html(status.news_count.jumlah);
		$('#clusters_count').html(status.clusters_count.jumlah);
	}

	function showLog(log) {
		var i = 0;
		if(table == null) {
			for(i=log.length-1; i >= 0; i--) {
				if(odd) {
					$('#content-table').prepend('<tr class="odd"><td>'+
						log[i].timestamp +'</td><td>'+
						log[i].tag +'</td><td>' + 
						log[i].aktivitas +' </td></tr>');
					odd = !odd;
				} else {
					$('#content-table').prepend('<tr class="even"><td>'+
						log[i].timestamp +'</td><td>'+
						log[i].tag +'</td><td>' + 
						log[i].aktivitas +' </td></tr>');
					odd = !odd;
				}
			}
			table = $('#dataTables-log').dataTable({
				"order": [ 0, 'desc' ]
			});
		} else {
			for(i=log.length-1; i >= 0; i--) {
				if(table.fnFindCellRowIndexes( log[i].timestamp, 0 ).length == 0 )
					table.api().row.add( [ log[i].timestamp, log[i].tag, log[i].aktivitas ]).draw();
			}
		}
	}

	function setConfig() {
		if(window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				console.log("Result : " + xmlhttp.responseText);
				var text = xmlhttp.responseText;
				console.log(text);
				data = JSON.parse(text);
				
				if(data.status == "success") {
					$('#status-config').html("");
					firtsTime = true;
					loadData();
					$('#update-button').removeAttr('disabled');
				} else {
					
				}
			}
		  }
		xmlhttp.open("POST","php/Config.php",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("rss_time="+$('[name="rss_time"]').val()+"&cluster_time="+ $('[name="cluster_time"]').val());
		$('#status-config').html("Config ...");
		$('#update-button').attr('disabled','disabled');
	}

	return {loadData : loadData, 
		startLoad : startLoad, 
		start : start, 
		stop : stop,
		setConfig : setConfig };
})();