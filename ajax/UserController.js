var UserController = (function() {
	
	function login() {
		console.log("yes");
		var username = document.getElementById("username").value;
		var password = document.getElementById("password").value;
		console.log(username);
		if(window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				console.log("Result : " + xmlhttp.responseText);
				var text = xmlhttp.responseText;
				console.log(text);
				data = JSON.parse(text);
				
				if(data.status == "success") {
					window.location = "index.php";
				} else {
					
				}
			}
		  }
		xmlhttp.open("POST","php/Login.php",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("username="+username+"&password="+password);

	}

	function logout() {
		console.log("logout");
		if(window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				console.log("Result : " + xmlhttp.responseText);
				var text = xmlhttp.responseText;
				console.log(text);
				data = JSON.parse(text);
				
				if(data.status == "success") {
					document.getElementById("user-panel").innerHTML = '<span><a href="login.php">Login</a></span>';
				} else {
					
				}
			}
		  }
		xmlhttp.open("GET","php/Logout.php",true);
		xmlhttp.send();

	}

	function register() {
		console.log("yes");
		var username = document.getElementById("username").value;
		var password = document.getElementById("password").value;
		var email = document.getElementById("email").value;

		if(window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				console.log("Result : " + xmlhttp.responseText);
				var text = xmlhttp.responseText;
				console.log(text);
				data = JSON.parse(text);
				
				if(data.status == "success") {
					window.location = "index.php";
				} else {
					
				}
			}
		  }
		xmlhttp.open("POST","php/Register.php",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("username="+username+"&password="+password+"&email="+email);
	}

	return {login : login, register : register, logout : logout};
})();