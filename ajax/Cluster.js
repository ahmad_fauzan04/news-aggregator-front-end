var Cluster = (function() {
	
	var pathName = document.URL;
	
	function loadCluster() {
		var id = getIDFromPathName();
		if(window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				console.log("Result : " + xmlhttp.responseText);
				var text = xmlhttp.responseText.replace(/\n/g,"<br/>");
				console.log(text);
				data = JSON.parse(text);
				
				if(data.status == "success") {
					generateCluster(data.cluster);
				} else {
				
				}
			}
		  }
		xmlhttp.open("GET","php/GetCluster.php?id="+ id,true);
		xmlhttp.send();
	}
	
	function getIDFromPathName() {
		var res = pathName.split("?");
		console.log(res);
		var parameter = res[1];
		var idIdx = parameter.indexOf("id=");
		return parseInt(parameter.substr(idIdx+3));
	}
	
	function generateCluster(clusterData) {
		var cluster_name = document.getElementById("cluster-name");
		var ringkasan = document.getElementById("ringkasan");
		var what_new = document.getElementById("whatnew");
		var list_artikel = document.getElementById("list-artikel");
		var popbox = document.getElementById("popbox");
		var image = document.getElementById("image-holder");
		var i =0;
		var artikel;
		
		// Generate Cluster description from artikel

		var info = clusterData.medoid;

		document.title = clusterData.CLUSTER_NAME;
		// document.title = info.INFO_WHAT;
		
		cluster_name.innerHTML = clusterData.CLUSTER_NAME;
		//cluster_name.innerHTML = info.INFO_WHAT;
		

		//ringkasan.innerHTML = clusterData.RINGKASAN_STANDARD;
		var stringhtml = "<p>";
		if(info.INFO_WHEN != "-") {
			stringhtml += "Pada " + info.INFO_WHEN+",";
		}

		if(info.INFO_WHO != "-") {
			stringhtml += " " + info.INFO_WHO+"";
		}		

		if(info.INFO_WHAT != "-") {
			stringhtml += " " + info.INFO_WHAT+"";
		}

		if(info.INFO_WHERE != "-") {
			stringhtml += " di " + info.INFO_WHERE+"";
		}
		
		if(info.INFO_WHY != "-") {
			stringhtml += " karena " + info.INFO_WHY+"";
		}

		if(info.INFO_HOW != "-") {
			stringhtml += " dengan " + info.INFO_HOW+",";
		}
		stringhtml += "</p>";
		ringkasan.innerHTML = stringhtml;

		// End of Generated
		// Add what new
		// Add Image
		image.innerHTML = "<img alt='#' src='"+ info.LOKASIFILE +"'/>";
		
		// Add list of artikel
		list_artikel.innerHTML = "";
		popbox.innerHTML = "";
		for(i=0; i < clusterData.artikel.length; i++) {
			artikel = clusterData.artikel[i];
			list_artikel.innerHTML += "<div class='list-item'>";
			list_artikel.innerHTML += "<h4 class='popper' data-popbox='art"+artikel.ID_ARTIKEL+"'>"+artikel.JUDUL+"</h4>";
			list_artikel.innerHTML += "<p>"+moment(artikel.TGL_TERBIT,"YYYY-MM-DD hh:mm:ss").fromNow()+"</p>"
			if(artikel.URL != null)
				list_artikel.innerHTML += "<p><a href='"+artikel.URL+"' onclick='Controller.gotoArtikel("+artikel.ID_ARTIKEL+")' target='_blank'>"+artikel.URL.substr(0,60)+"</a></p>";// add popbox	
			list_artikel.innerHTML += "<hr/>"
			var stringhtml = "<div id='art"+artikel.ID_ARTIKEL+"' class='popbox' style='width:25%; font-size:1.5em;'>";
			if(artikel.INFO_WHO != null)
				stringhtml += "<p><b>Who?</b> "+artikel.INFO_WHO+"</p>";
			if(artikel.INFO_WHEN != null)
				stringhtml += "<p><b>When?</b> "+artikel.INFO_WHEN+"</p>";
			if(artikel.INFO_WHAT != null)
				stringhtml += "<p><b>What?</b> "+artikel.INFO_WHAT+"</p>";
			if(artikel.INFO_WHERE != null)
				stringhtml += "<p><b>Where?</b> "+artikel.INFO_WHERE+"</p>";
			if(artikel.INFO_WHY != null)
				stringhtml += "<p><b>Why?</b> "+artikel.INFO_WHY+"</p>";
			if(artikel.INFO_HOW != null)
				stringhtml += "<p><b>How?</b> "+artikel.INFO_HOW+"</p></div>";
			popbox.innerHTML += stringhtml;
		}
		// Add hover information
		PopBox.init();
	}
	
	return {loadCluster : loadCluster};
})();