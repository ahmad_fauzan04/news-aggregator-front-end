var Kategori = (function() {
	var pathName = document.URL;
	var lastCluster = 0;
	var numberClusterPerPage = 5;
	
	function loadKategori() {
		var id = getIDFromPathName();
		$('#menu-'+id).addClass("menu-toc-current");
		$('#menu-small-'+id).addClass("active");
		if(window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		function downloadJSAtOnload() {
			var element = document.createElement("script");
			element.src = "js/page.js";
			document.body.appendChild(element);
		}

		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				console.log("Result : " + xmlhttp.responseText);
				var text = xmlhttp.responseText.replace(/\n/g,"<br/>");
				console.log(text);
				data = JSON.parse(text);
				
				if(data.status == "success") {
					generateKategoriContent(data.kategori);
					downloadJSAtOnload();
					PopBox.init();
					Page.init();
					Page.update();
					setTimeout(function(){}, 0);
				} else {
				
				}
			}
		}
		
		xmlhttp.open("GET","php/GetKategori.php?id="+ id +"&from="+ lastCluster,true);
		xmlhttp.send();		
	}
	
	function getIDFromPathName() {
		var res = pathName.split("?");
		console.log(res);
		var parameter = res[1];
		var idIdx = parameter.indexOf("id=");
		return parseInt(parameter.substr(idIdx+3));
	}
	
	function generateKategoriContent(kategoriData) {
		document.title = 'Kategori - ' + kategoriData.LABEL;
		var i=0,j=0;
		var clusterIdx = 0;
		var block = document.getElementById("bb-bookblock");
		
		var length = Math.floor(kategoriData.cluster.length/numberClusterPerPage);
		if(kategoriData.cluster.length % numberClusterPerPage > 0) {
			length += 1;
		}
		
		block.innerHTML = "";
		var stringhtml = "";
		for(i=0; i < length; i++) {
			stringhtml += "<div class='bb-item' id='item"+(i+1)+"'>";
			stringhtml += generateHeader(kategoriData);
			stringhtml += "<div class='content1'><div class='news_list'>";
			for(clusterIdx=0; clusterIdx < (i+1)*numberClusterPerPage && clusterIdx < kategoriData.cluster.length; clusterIdx++) {
				stringhtml += generateCluster(kategoriData.cluster[clusterIdx]);
			}
			stringhtml += "</div></div></div>"
		}
		block.innerHTML += stringhtml;
		console.log(stringhtml);
		console.log(kategoriData.cluster.length);
		
	}
	
	function generateHeader(kategoriData) {
		var stringhtml = "<div class='header'><h1 id='kategori_name'>"+kategoriData.LABEL+"</h1></div>";
		return stringhtml;
	}
	
	function generateCluster(clusterData) {
		var i =0;
		var artikel;
		var stringhtml = "<div class='news' id='cluster-"+clusterData.ID_CLUSTER+"'>";
		var info = clusterData.medoid;
			
		stringhtml += "<img alt=''  src='"+info.LOKASIFILE+"'>";

		//stringhtml += "<a href='cluster.php?id="+clusterData.ID_CLUSTER+"'><h2 >"+clusterData.CLUSTER_NAME+"</h2></a>";
		var cluster_title = "";
		var cluster_title_words = clusterData.CLUSTER_NAME.split(' ');
		if(cluster_title_words.length <= 8) {
			cluster_title = clusterData.CLUSTER_NAME;
		} else {
			for(i=0; i < 8; i++) {
				cluster_title += cluster_title_words[i] + " ";
			}
			cluster_title += "...";
		}
		stringhtml += "<a href='cluster.php?id="+clusterData.ID_CLUSTER+"' style='color: #428bca;'><h2 >"+cluster_title+"</h2></a>";
		
		// stringhtml += "<p>"+clusterData.RINGKASAN_STANDARD+"</p>";
		
		stringhtml += "<p>";
		if(info.INFO_WHEN != "-") {
			stringhtml += "Pada " + info.INFO_WHEN+",";
		}

		if(info.INFO_WHO != "-") {
			stringhtml += " " + info.INFO_WHO+"";
		}		

		if(info.INFO_WHAT != "-") {
			stringhtml += " " + info.INFO_WHAT+"";
		}

		if(info.INFO_WHERE != "-") {
			stringhtml += " di " + info.INFO_WHERE+"";
		}
		
		if(info.INFO_WHY != "-") {
			stringhtml += " karena " + info.INFO_WHY+"";
		}

		if(info.INFO_HOW != "-") {
			stringhtml += " dengan " + info.INFO_HOW+",";
		}
		stringhtml += "</p>";
		
		// END OF Generate

		if(clusterData.RINGKASAN_WHATNEW != undefined) {
			stringhtml += "<p><i>"+clusterData.RINGKASAN_WHATNEW+"</i></p>";
		}
		stringhtml += "<div class='list'>";
		
		for(i=0; i < clusterData.artikel.length; i++) {
			artikel = clusterData.artikel[i];
			stringhtml += "<div class='list-item' id='artikel-"+artikel.ID_ARTIKEL+"'>";
			stringhtml += "<h4 class='popper' data-popbox='art"+artikel.ID_ARTIKEL+"'><!--<img alt='#'  src='img/icon/glyphicons_check.png'>-->"+artikel.JUDUL+"</h4>";
			stringhtml += "<p>"+moment(artikel.TGL_TERBIT,"YYYY-MM-DD hh:mm:ss").fromNow()+"</p>"
			if(artikel.URL != null)
				stringhtml += "<p><a href='"+artikel.URL+"' onclick='Controller.gotoArtikel("+artikel.ID_ARTIKEL+")' target='_blank'>"+artikel.URL.substr(0,45)+"</a></p>";
			stringhtml += "<div id='art"+artikel.ID_ARTIKEL+"' class='popbox' >";
			stringhtml += "<p><b>Who?</b> "+artikel.INFO_WHO+"</p>";
			stringhtml += "<p><b>When?</b> "+artikel.INFO_WHEN+"</p>";
			stringhtml += "<p><b>What?</b> "+artikel.INFO_WHAT+"</p>";
			stringhtml += "<p><b>Where?</b> "+artikel.INFO_WHERE+"</p>";
			stringhtml += "<p><b>Why?</b> "+artikel.INFO_WHY+"</p>";
			stringhtml += "<p><b>How?</b> "+artikel.INFO_HOW+"</p>";
			stringhtml += "</div>";
			stringhtml += "</div>";
		}
		stringhtml += "</div>";
		stringhtml += "</div>";
		
		return stringhtml;
	}
	
	return {loadKategori : loadKategori};
})();
