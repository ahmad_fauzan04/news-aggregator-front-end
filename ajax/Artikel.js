var Artikel = (function() {
	var pathName = document.URL;
	
	function loadArtikel() {
		var id = getIDFromPathName();
		if(window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				console.log("Result : " + xmlhttp.responseText);
				var text = xmlhttp.responseText.replace(/\n/g,"<br/>");
				console.log(text);
				data = JSON.parse(text);
				
				if(data.status == "success") {
					generateArtikel(data.artikel);
				} else {
				
				}
			}
		  }
		xmlhttp.open("GET","php/GetArtikel.php?id="+ id,true);
		xmlhttp.send();
	}
	
	function getIDFromPathName() {
		var res = pathName.split("?");
		console.log(res);
		var parameter = res[1];
		var idIdx = parameter.indexOf("id=");
		return parseInt(parameter.substr(idIdx+3));
	}
	
	function generateArtikel(artikel) {
		var title = document.getElementById("title");
		var img = document.getElementById("image");
		var text = document.getElementById("text");
		var paragraphs = artikel.FULL_TEXT.split("\r\n");
		var source = document.getElementById("source");
		var info = document.getElementById("info");
		var i=0;
		
		title.innerHTML = artikel.JUDUL;
		img.src = artikel.LOKASIFILE;
		
		text.innerHTML = "";
		for(i=0; i < paragraphs.length; i++) {
			text.innerHTML += "<p>" + paragraphs[i] + "</p>";
		}
		source.innerHTML = "<em>Sumber <a href='"+artikel.URL+"' target='_blank'>"+ artikel.JUDUL +"</a></em>";
		
		// Initialize the scroller
		var $content = $( '.content2' ),apiJSP = $content.data( 'jsp' );
		
		if(apiJSP === undefined) {
			$content.jScrollPane({verticalGutter : 0, hideFocus : true });
		} else {
			apiJSP.reinitialise();
		}
		
		
		info.innerHTML = "";
		info.innerHTML += "<p><b>Who?</b> " + artikel.INFO_WHO +"</p>";
		info.innerHTML += "<p><b>When?</b> "+ artikel.INFO_WHEN +"</p>";
		info.innerHTML += "<p><b>What?</b> "+ artikel.INFO_WHAT +"</p>";
		info.innerHTML += "<p><b>Where?</b> "+ artikel.INFO_WHERE +"</p>";
		info.innerHTML += "<p><b>Why?</b> " + artikel.INFO_WHY +"</p>";
		info.innerHTML += "<p><b>How?</b> " + artikel.INFO_HOW +"</p>";
	}

	function searchArtikel() {
		var keyword = getKeyword();
		if(window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				console.log("Result : " + xmlhttp.responseText);
				var text = xmlhttp.responseText.replace(/\n/g,"<br/>");
				console.log(text);
				data = JSON.parse(text);
				
				if(data.status == "success") {
					for(var i=0; i < data.artikel.length; i++){
						generateSearchResult(data.artikel[0]);
					}
				} else {
				
				}
			}
		  }
		xmlhttp.open("GET","php/Search.php?keyword="+ keyword,true);
		xmlhttp.send();
	}

	function generateSearchResult(artikel) {
		var result_el = document.getElementById("result");
		var stringHTML = '<div class="search-result">';
		stringHTML += '<h3>'+artikel.JUDUL+'</h3>';
		stringHTML += '<a href="'+artikel.URL+'">'+artikel.URL+'</a>';
		stringHTML += '<p> (who) '+artikel.INFO_WHO+'. (when) '+artikel.INFO_WHEN+'. (where) '+artikel.INFO_WHERE+'. (why) '+artikel.INFO_WHY+'.</p>';
		stringHTML += '<hr/>';
		stringHTML += '</div>';
		result_el.innerHTML += stringHTML;
	}

	function getKeyword() {
		var res = pathName.split("?");
		console.log(res);
		var parameter = res[1];
		var idIdx = parameter.indexOf("keyword=");
		return parameter.substr(idIdx+8);
	}

	return {loadArtikel : loadArtikel, searchArtikel : searchArtikel};
	
})();