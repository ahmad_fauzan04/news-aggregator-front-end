<div id="menu-panel" class="menu-panel">
	<h3>News Aggregator</h3>
	<ul id="menu-toc" class="menu-toc">
		<li id="menu-0"><a href="index.php"><img alt="#"  src="img/icon/glyphicons_home.png"/> Home</a></li>
		<li id="menu-1" ><a href="category.php?id=1"><img alt="#"  src="img/icon/glyphicons_education.png"/> Pendidikan</a></li>
		<li id="menu-2" ><a href="category.php?id=2"><img alt="#"  src="img/icon/glyphicons_politics.png"/> Politik</a></li>
		<li id="menu-3" ><a href="category.php?id=3"><img alt="#"  src="img/icon/glyphicons_criminal.png"/> Hukum dan Kriminal</a></li>
		<li id="menu-4" ><a href="category.php?id=4"><img alt="#"  src="img/icon/glyphicons_social.png"/> Sosial Budaya</a></li>
		<li id="menu-5" ><a href="category.php?id=5"><img alt="#"  src="img/icon/glyphicons_sports.png"/> Olahraga</a></li>
		<li id="menu-6" ><a href="category.php?id=6"><img alt="#"  src="img/icon/glyphicons_techs.png"/> Teknologi dan Sains</a></li>
		<li id="menu-7" ><a href="category.php?id=7"><img alt="#"  src="img/icon/glyphicons_play.png"/> Hiburan</a></li>
		<li id="menu-8" ><a href="category.php?id=8"><img alt="#"  src="img/icon/glyphicons_bussiness.png"/> Bisnis dan Ekonomi</a></li>
		<li id="menu-9" ><a href="category.php?id=9"><img alt="#"  src="img/icon/glyphicons_health.png"/> Kesehatan</a></li>
		<li id="menu-10" ><a href="category.php?id=10"><img alt="#"  src="img/icon/glyphicons_disaster.png"/> Bencana</a></li>
	</ul>
	
	<div class="bottom">
		<a href="login.php">Login</a>
	</div>
</div>

