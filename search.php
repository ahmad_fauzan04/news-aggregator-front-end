<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>Search - Kemacetan di Bandung</title>
		<meta name="description" content="News Aggregator" />
		<meta name="keywords" content="fullscreen pageflip, booklet, layout, bookblock, jquery plugin, flipboard layout, sidebar menu" />
		<meta name="author" content="Fauzan" />
		<link rel="stylesheet" type="text/css" href="css/jquery.jscrollpane.custom.css" />
		<link rel="stylesheet" type="text/css" href="css/bookblock.css" />
		<link rel="stylesheet" type="text/css" href="css/popbox.css" />
		<link rel="stylesheet" type="text/css" href="css/custom.css" />
		<script src="js/modernizr.custom.79639.js"></script>
	</head>
	<body>
		<div id="container" class="container">	

			<?php include 'menu.php'; ?>
			
			<div class="bb-custom-wrapper">
				<div id="bb-bookblock" class="bb-bookblock">
					<div class="bb-item" id="item1">
						<div class="content2">
							<div class="scroller">
									<h1> Search for "<?php echo $_GET['keyword']; ?>" </h2>
									<hr/>
									<br/>
									<div id="result">
										
									<div id="result">
							</div>
						</div>
					</div>
				</div>
				
				<div id="popbox">
					
				</div>
				
				<footer>
				Laboratorium Grafika dan Intelegensia Buatan, Institut Teknologi Bandung <br/>
				icon from <a href="http://glyphicons.com">glyphicons.com</a>
				</footer>
		
				<nav>
					<div id="bb-nav-prev" class="left">&larr;</div>
					<div id="bb-nav-next" class="right">&rarr;</div>
				</nav>
				
				<div class="side-nav">
					<div id="tblcontents" class="menu-item">&#9776;</div>
					<hr>
					<div class="menu-item"><abbr title="home"><img alt="#"  src="img/icon/glyphicons_home.png"/></abbr></div>
					<div class="menu-item"><abbr title="politics"><img alt="#"  src="img/icon/glyphicons_politics.png"/></abbr></div>
					<div class="menu-item"><abbr title="traffics"><img alt="#"  src="img/icon/glyphicons_traffics.png"/></abbr></div>
					<div class="menu-item"><abbr title="social"><img alt="#"  src="img/icon/glyphicons_social.png"/></abbr></div>
					<div class="menu-item"><abbr title="business"><img alt="#"  src="img/icon/glyphicons_bussiness.png"/></abbr></div>
					<div class="menu-item active"><abbr title="nature"><img alt="#"  src="img/icon/glyphicons_nature.png"/></abbr></div>
					<div class="menu-item"><abbr title="photography"><img alt="#"  src="img/icon/glyphicons_photography.png"/></abbr></div>
					<div class="menu-item"><abbr title="politics"><img alt="#"  src="img/icon/glyphicons_politics.png"/></abbr></div>
					<div class="menu-item"><abbr title="traffics"><img alt="#"  src="img/icon/glyphicons_traffics.png"/></abbr></div>
					<div class="menu-item"><abbr title="social"><img alt="#"  src="img/icon/glyphicons_social.png"/></abbr></div>
					<div class="menu-item"><abbr title="business"><img alt="#"  src="img/icon/glyphicons_bussiness.png"/></abbr></div>
					<hr>
					<div class="menu-item"><abbr title="more"><img alt="#"  src="img/icon/glyphicons_more.png"/></abbr></div>
				</div>
				
				<div class="search-nav">
					<form id="search-form" name="search" action="/products" method="get">
						<input id="search-input" name="search" type="text" placeholder="search news">
						<input class="button" src="img/search-icon.png" name="submit" type="image" alt="#">
					</form>
				</div>
				
			</div>
				
		</div><!-- /container -->
		
		
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script src="js/jquery.mousewheel.js"></script>
		<script src="js/jquery.jscrollpane.min.js"></script>
		<script src="js/jquerypp.custom.js"></script>
		<script src="js/jquery.bookblock.js"></script>
		<script src="js/page.js"></script>
		<script src="ajax/Artikel.js"></script>
		<script>
			$(function() {
				Artikel.searchArtikel();
				Page.init();
			});
		</script>
		<script src="js/jquery.popbox.js"></script>
	</body>
</html>
