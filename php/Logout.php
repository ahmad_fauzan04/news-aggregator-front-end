<?php
	session_start();
	
	$response = array();

	if(isset($_SESSION['userid'])) {
		unset($_SESSION['userid']);
		unset($_SESSION['username']);
		$response['status'] = 'success';
	} else {
		$response['status'] = 'fail';
	}

	echo json_encode($response);
?>