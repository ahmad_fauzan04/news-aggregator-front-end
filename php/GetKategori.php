<?php
	include ("Header.php");
	mysql_set_charset("utf8");

	$response = array();
	
	if(isset($_GET["id"])) {
		$id = $_GET["id"];
		$from = 0;
		if(isset($_GET["from"])) {
			$from = $_GET["from"];
		}
		$query = "SELECT * FROM kategori WHERE ID_KELAS=$id";

		$result = mysql_query($query,$link);
		if(mysql_num_rows($result) > 0) {
			$response['status'] = "success";
			$response['kategori'] = mysql_fetch_array($result);
			
			$response['kategori']['cluster'] = array();
			$query = "SELECT DISTINCT cluster.* FROM kategori,artikel_kategori,cluster_artikel,cluster 
			WHERE kategori.ID_KELAS=artikel_kategori.ID_KELAS AND	artikel_kategori.ID_ARTIKEL=cluster_artikel.ID_ARTIKEL AND
			cluster_artikel.ID_CLUSTER=cluster.ID_CLUSTER
			AND kategori.ID_KELAS=$id LIMIT $from,20";
			

			$result = mysql_query($query,$link);
			if(mysql_num_rows($result) > 0) {
				
				while($row = mysql_fetch_array($result)) {
					$cluster = $row;
					$id_cluster = $row['ID_CLUSTER'];
					$cluster['artikel'] = array();
					$query = "SELECT * FROM cluster_artikel natural join artikel natural join gambar_artikel WHERE ID_CLUSTER=$id_cluster ORDER BY TGL_TERBIT DESC";
					
					$result2 = mysql_query($query,$link);
					while($artikel = mysql_fetch_array($result2)) {
						$cluster['artikel'][] = $artikel;

					}

					// Add Medoid
					$query = "SELECT * FROM (cluster inner join artikel ON cluster.ID_MEDOID = artikel.ID_ARTIKEL) LEFT JOIN gambar_artikel on artikel.ID_ARTIKEL=gambar_artikel.ID_ARTIKEL WHERE ID_CLUSTER=$id_cluster";
					$result2 = mysql_query($query,$link);
					if(mysql_num_rows($result2) > 0) {
						$row = mysql_fetch_array($result2);
						$cluster['medoid'] = $row;
					}

					$response['kategori']['cluster'][] = $cluster;
				}
			}
		}		else {
			$response['status'] = "not found";
		}
	} else {
		$response['status'] = "failed";
	}

	echo json_encode($response);
?>