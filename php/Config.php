<?php
	include ("Header.php");

	$response = array();

	if(isset($_SESSION["role"]) && $_SESSION["role"] == "admin") {
		if(isset($_POST['status'])) {
			$status = $_POST['status'];
			$query = "UPDATE config SET status=$status";
			$result = mysql_query($query,$link);
			if($result) {
				$response['status'] = "success";
			} else {
				$response['status'] = "failed";
			}
		}

		if(isset($_POST['rss_time'])) {
			$rss_time = $_POST['rss_time'];

			$query = "UPDATE config SET durasi_parser=$rss_time";
			$result = mysql_query($query,$link);
			if($result) {
				$response['status'] = "success";
			} else {
				$response['status'] = "failed";
			}
		}

		if(isset($_POST['cluster_time'])) {
			$cluster_time = $_POST['cluster_time'];

			$query = "UPDATE config SET durasi_cluster=$cluster_time";
			$result = mysql_query($query,$link);
			if($result) {
				$response['status'] = "success";
			} else {
				$response['status'] = "failed";
			}
		}


	} else {
		$response['status'] = "failed";
		$response['message'] = "Authentication Failed";
	}

	echo json_encode($response);

?>
