<?php
	include ("Header.php");

	$response = array();

	if(isset($_SESSION["role"]) && $_SESSION["role"] == "admin") {
		// Load Config
		$query = "SELECT * FROM config";
		$result = mysql_query($query,$link);
		if(mysql_num_rows($result) > 0) {
			$response['status'] = "success";
			$response['config'] = mysql_fetch_array($result);
		}

		// Load Status

		$response['status_crawler'] = array();

		// Load number of news
		$query = "SELECT COUNT(*) as jumlah FROM artikel";
		$result = mysql_query($query,$link);
		if(mysql_num_rows($result) > 0) {
			$response['status'] = "success";
			$response['status_crawler']['news_count'] = mysql_fetch_array($result);
		}

		// Load number of cluster
		$query = "SELECT COUNT(*) as jumlah FROM cluster";
		$result = mysql_query($query,$link);
		if(mysql_num_rows($result) > 0) {
			$response['status'] = "success";
			$response['status_crawler']['clusters_count'] = mysql_fetch_array($result);
		}

		// Load Log 20 last log
		$response['log'] = array();
		$query = "SELECT * FROM log ORDER BY timestamp DESC LIMIT 0,20";
		$result = mysql_query($query,$link);
		if(mysql_num_rows($result) > 0) {
			while($row = mysql_fetch_array($result)) {
				$response['log'][] = $row;
			}
		}
	} else {
		$response['status'] = "failed";
		$response['message'] = "Authentication Failed";
	}

	echo json_encode($response);
?>