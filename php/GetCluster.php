<?php
	include ("Header.php");
	mysql_set_charset("utf8");
	
	$response = array();
	
	if(isset($_GET["id"])) {
		$id = $_GET["id"];
		$query = "SELECT * FROM cluster WHERE ID_CLUSTER=$id";
		$result = mysql_query($query,$link);
		if(mysql_num_rows($result) > 0) {
			$response['status'] = "success";
			$response['cluster'] = mysql_fetch_array($result);
			// Get Artikel
			$response['cluster']['artikel'] = array();
			$query = "SELECT * FROM cluster_artikel natural join artikel natural join gambar_artikel WHERE ID_CLUSTER=$id ORDER BY TGL_TERBIT DESC";
			$result = mysql_query($query,$link);
			while($row = mysql_fetch_array($result)) {
				$response['cluster']['artikel'][] = $row;
			}

			// Add Medoid
			$query = "SELECT * FROM (cluster inner join artikel ON cluster.ID_MEDOID = artikel.ID_ARTIKEL) LEFT JOIN gambar_artikel on artikel.ID_ARTIKEL=gambar_artikel.ID_ARTIKEL WHERE ID_CLUSTER=$id";
			$result = mysql_query($query,$link);
			if(mysql_num_rows($result) > 0) {
				$row = mysql_fetch_array($result);
				$response['cluster']['medoid'] = $row;
			}
		} else {
			$response['status'] = "not found";
		}
	} else {
		$response['status'] = "failed";
	}
	
	echo json_encode($response);
?>