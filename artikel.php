!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>Artikel - Puluhan Ribu Kendaraan Padati Puncak dan Bandung</title>
		<link rel="shortcut icon" href="img/icon/glyphicons_icon.png">
		<meta name="description" content="News Aggregator" />
		<meta name="keywords" content="fullscreen pageflip, booklet, layout, bookblock, jquery plugin, flipboard layout, sidebar menu" />
		<meta name="author" content="Fauzan" />
		<link rel="stylesheet" type="text/css" href="css/jquery.jscrollpane.custom.css" />
		<link rel="stylesheet" type="text/css" href="css/bookblock.css" />
		<link rel="stylesheet" type="text/css" href="css/popbox.css" />
		<link rel="stylesheet" type="text/css" href="css/custom.css" />
		<script src="js/modernizr.custom.79639.js"></script>
	</head>
	<body>
		<div id="container" class="container">	

			<div id="menu-panel" class="menu-panel">
				<h3>News Aggregator</h3>
				<ul id="menu-toc" class="menu-toc">
					<li class="menu-toc-current"><a href="index.php"><img alt="#"  src="img/icon/glyphicons_home.png"/> Home</a></li>
					<li><a href="#"><img alt="#"  src="img/icon/glyphicons_politics.png"/> Politics</a></li>
					<li ><a href="#"><img alt="#"  src="img/icon/glyphicons_traffics.png"/> Traffics</a></li>
					<li><a href="#"><img alt="#"  src="img/icon/glyphicons_social.png"/> Social</a></li>
					<li><a href="#"><img alt="#"  src="img/icon/glyphicons_bussiness.png"/> Business</a></li>
					<li><a href="#"><img alt="#"  src="img/icon/glyphicons_nature.png"/> Nature</a></li>
					<li><a href="#"><img alt="#"  src="img/icon/glyphicons_photography.png"/> Photography</a></li>
					<li><a href="#"><img alt="#"  src="img/icon/glyphicons_politics.png"/> Politics</a></li>
					<li><a href="#"><img alt="#"  src="img/icon/glyphicons_traffics.png"/> Traffics</a></li>
					<li><a href="#"><img alt="#"  src="img/icon/glyphicons_social.png"/> Social</a></li>
					<li><a href="#"><img alt="#"  src="img/icon/glyphicons_bussiness.png"/> Business</a></li>
					<li><a href="#"><img alt="#"  src="img/icon/glyphicons_nature.png"/> Nature</a></li>
					<li><a href="#"><img alt="#"  src="img/icon/glyphicons_photography.png"/> Photography</a></li>
				</ul>
				
				<div class="bottom">
					<a href="#">Login</a>
				</div>
			</div>

			<div class="bb-custom-wrapper">
				<div id="bb-bookblock" class="bb-bookblock">
					<div class="bb-item" id="item1">
						<div class="content2">
							<div class="scroller">
								<h2 id="title">Puluhan Ribu Kendaraan Padati Puncak dan Bandung</h2>
								<!--<p id="source"><i>oleh news.liputan6.com</i><p>
								<br>-->
								<img id="image" alt="#"  src="img/macet.jpg">
								
								<div id="text"></div>
								
								<p id="source"><em>Sumber <a href="http://news.liputan6.com/read/2056013/puluhan-ribu-kendaraan-padati-puncak-dan-bandung" target="_blank">Puluhan Ribu Kendaraan Padati Puncak dan Bandung</a></em></p>
							</div>
						</div>
						<div id="info" class="qa">
							
						</div>
					</div>
				</div>
				<footer>
				Laboratorium Grafika dan Intelegensia Buatan, Institut Teknologi Bandung <br/>
				icon from <a href="http://glyphicons.com">glyphicons.com</a>
				</footer>
		
				<nav>
					<div id="bb-nav-prev" class="left">&larr;</div>
					<div id="bb-nav-next" class="right">&rarr;</div>
				</nav>
				
				<div class="side-nav">
					<div id="tblcontents" class="menu-item">&#9776;</div>
					<hr>
					<div class="menu-item"><a href="index.php"><abbr title="home"><img alt="#"  src="img/icon/glyphicons_home.png"/></abbr></a></div>
					<div class="menu-item"><abbr title="politics"><img alt="#"  src="img/icon/glyphicons_politics.png"/></abbr></div>
					<div class="menu-item"><abbr title="traffics"><img alt="#"  src="img/icon/glyphicons_traffics.png"/></abbr></div>
					<div class="menu-item"><abbr title="social"><img alt="#"  src="img/icon/glyphicons_social.png"/></abbr></div>
					<div class="menu-item"><abbr title="business"><img alt="#"  src="img/icon/glyphicons_bussiness.png"/></abbr></div>
					<div class="menu-item active"><abbr title="nature"><img alt="#"  src="img/icon/glyphicons_nature.png"/></abbr></div>
					<div class="menu-item"><abbr title="photography"><img alt="#"  src="img/icon/glyphicons_photography.png"/></abbr></div>
					<div class="menu-item"><abbr title="politics"><img alt="#"  src="img/icon/glyphicons_politics.png"/></abbr></div>
					<div class="menu-item"><abbr title="traffics"><img alt="#"  src="img/icon/glyphicons_traffics.png"/></abbr></div>
					<div class="menu-item"><abbr title="social"><img alt="#"  src="img/icon/glyphicons_social.png"/></abbr></div>
					<div class="menu-item"><abbr title="business"><img alt="#"  src="img/icon/glyphicons_bussiness.png"/></abbr></div>
					<hr>
					<div class="menu-item"><abbr title="more"><img alt="#"  src="img/icon/glyphicons_more.png"/></abbr></div>
				</div>
				
				<div class="search-nav">
					<form id="search-form" name="search" action="/products" method="get">
						<input id="search-input" name="search" type="text" placeholder="search news">
						<input class="button" src="img/search-icon.png" name="submit" type="image" alt="">
					</form>
				</div>
				
			</div>
				
		</div><!-- /container -->
		
		
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script src="js/jquery.mousewheel.js"></script>
		<script src="js/jquery.jscrollpane.min.js"></script>
		<script src="js/jquerypp.custom.js"></script>
		<script src="js/jquery.bookblock.js"></script>
		<script src="js/jquery.popbox.js"></script>
		<script src="js/page.js"></script>
		<script src="ajax/Artikel.js"></script>
		<script>
			$(function() {

				Page.init();
				Artikel.loadArtikel();
			});
		</script>
	</body>
</html>
