<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>News Aggregator - Home</title>
		<link rel="shortcut icon" href="img/icon/glyphicons_icon.png">
		<meta name="description" content="News Aggregator" />
		<meta name="keywords" content="fullscreen pageflip, booklet, layout, bookblock, jquery plugin, flipboard layout, sidebar menu" />
		<meta name="author" content="Fauzan" />
		<link rel="stylesheet" type="text/css" href="css/jquery.jscrollpane.custom.css" />
		<link rel="stylesheet" type="text/css" href="css/bookblock.css" />
		<link rel="stylesheet" type="text/css" href="css/popbox.css" />
		<link rel="stylesheet" type="text/css" href="css/home.css" />
		<script src="js/modernizr.custom.79639.js"></script>
	</head>
	<body>
		<div id="container" class="container slideRight">	

			<?php include 'menu.php'; ?>

			<div class="bb-custom-wrapper">
				<div id="bb-bookblock" class="bb-bookblock" style="perspective: 2000px;">
					<div class="bb-item" id="item1" style="display: block;">
						<div class="content home">
							<h1>Popular</h1>
							<div id="popular" class="trend">
								
							</div>
							<div id="popular2" class="trend">
								
							</div>
						</div>
						
					</div>
					
				</div>
				
				<footer>
				Laboratorium Grafika dan Intelegensia Buatan, Institut Teknologi Bandung <br/>
				icon from <a href="http://glyphicons.com">glyphicons.com</a>
				</footer>
				
				<div class="search-nav">
					<div id="user-panel">
						<?php
							session_start();
							if(isset($_SESSION['username'])) {
						?>
								<span class="username">
									<?php echo $_SESSION['username']; ?>
								</span>
								<span>
									<a href="#" onclick="UserController.logout()">Logout</a>
								</span>
						<?php
							} else {
						?>
							<span>
								<a href="login.php">Login</a>
							</span>
						<?php
							}
						?>

						<?php if(isset($_SESSION["role"]) && $_SESSION["role"] == "admin") { ?>
							<span>
								<a href="admin.php" > Admin Page</a>
							</span>
						<?php } ?>
					</div>
					<form id="search-form" name="search" action="/products" method="get">
						<input id="search-input" name="search" type="text" placeholder="search news">
						<input class="button" src="img/search-icon.png" name="submit" type="image" alt="#">
					</form>
				</div>
				
			</div>
				
		</div><!-- /container -->
		
		
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script src="js/jquery.mousewheel.js"></script>
		<script src="js/jquery.jscrollpane.min.js"></script>
		<script src="js/jquerypp.custom.js"></script>
		<script src="js/home.js"></script>
		<script src="js/moment.min.js"></script>
		<script src="ajax/UserController.js"></script>
		<script src="ajax/Popular.js"></script>
		<script>
			$(function() {

				Home.init();
				Popular.loadPopular();
			});
		</script>
	</body>
</html>
