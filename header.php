<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">#BandungMengeluh</a>
		</div>
		
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		
		<ul class="nav navbar-nav navbar-right">
			<?php
				$login = false;
				if(isset($_SESSION["username"])) {
					$login = true;
			?>
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION["username"] ?> <b class="caret"></b></a>
				  <ul class="dropdown-menu">
					<?php 
						if($_SESSION['role']==1) {
					?>
						<li><a href="admin.php">Admin Page</a></li>
					<?php } else if($_SESSION['role']==3) { ?>
						<li><a href="admin-pemerintah.php">Admin Page</a></li>
					<?php } ?>
					<li class="divider"></li>
					<li><a href="#" onclick="logout()">Logout</a></li>
				  </ul>
				</li>
			<?php 
				} else {
			?>
			<li ><a href='register.php'>Register</a></li>
			<li ><button type="button" class="btn btn-primary navbar-btn" onclick="window.location.href='login.php'">Sign in</button></li>
			<?php 
				}
			?>
		</ul>
	</div>
	</div>
</nav>