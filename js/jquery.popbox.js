// http://www.sundoginteractive.com/sunblog/posts/jquery-hover-box
var PopBox = (function() {
	
	function init() {
		var moveLeft = 0;
		var moveDown = 0;
		$('.popper').hover(function(e) {
	   
			var target = '#' + ($(this).attr('data-popbox'));
			 
			$(target).show();
			var position = $(this).position();
			moveLeft = position.left + $(this).outerWidth();
			moveDown = position.top - ($(target).outerHeight() / 2);
			/*if(($(window).width()-$(target).outerWidth()) < (moveLeft + $(target).outerWidth() )) {
				moveLeft = position.left - $(target).outerWidth() ;
			}*/

            if(($(window).height()-$(target).outerHeight()) < (moveDown + $(target).outerHeight()/2 )) {
                moveDown = $(window).height() - $(target).outerHeight() ;
            }

			$(target).css('top', moveDown).css('left', moveLeft);
		}, function() {
			var target = '#' + ($(this).attr('data-popbox'));
			$(target).hide();
		});
	 }
    /*$('.popper').mousemove(function(e) {
        var target = '#' + ($(this).attr('data-popbox'));
         
        leftD = e.pageX + parseInt(moveLeft);
        maxRight = leftD + $(target).outerWidth();
        windowLeft = $(window).width() - 40;
        windowRight = 0;
        maxLeft = e.pageX - (parseInt(moveLeft) + $(target).outerWidth() + 20);
         
        if(maxRight > windowLeft && maxLeft > windowRight)
        {
            leftD = maxLeft;
        }
     
        topD = e.pageY - parseInt(moveDown);
        maxBottom = parseInt(e.pageY + parseInt(moveDown) + 20);
        windowBottom = parseInt(parseInt($(document).scrollTop()) + parseInt($(window).height()));
        maxTop = topD;
        windowTop = parseInt($(document).scrollTop());
        if(maxBottom > windowBottom)
        {
            topD = windowBottom - $(target).outerHeight() - 20;
        } else if(maxTop < windowTop){
            topD = windowTop + 20;
        }
     
        $(target).css('top', topD).css('left', leftD);
     
     
    });*/
	return {init : init};
})();