var Home = (function() {

	var $menu_panel = $('.menu-toc'), current = 0;

	function init() {

		// initialize jScrollPane on the content div of the first item
		setJSP( 'init' );
		initEvents();

	}
	
	function initEvents() {


		// reinit jScrollPane on window resize
		$( window ).on( 'debouncedresize', function() {
			// reinitialise jScrollPane on the content div
			setJSP( 'reinit' );
		} );

	}

	function setJSP( action, idx) {
		
		var idx = idx === undefined ? current : idx,
			apiJSP = $menu_panel.data( 'jsp' );
		
		if( action === 'init' && apiJSP === undefined ) {
			$menu_panel.jScrollPane();
		}
		else if( action === 'reinit' && apiJSP !== undefined ) {
			apiJSP.reinitialise();
		}
		else if( action === 'destroy' && apiJSP !== undefined ) {
			apiJSP.destroy();
		}
		
	}

	return { init : init };

})();