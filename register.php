<html>
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>News Aggregator - Register</title>
		<link rel="shortcut icon" href="img/icon/glyphicons_icon.png">
		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		
		<link href="css/signin.css" rel="stylesheet">
	</head>
	<body>
		<div class="container">

		  <div class="form-signin" role="form">
			<h2 class="form-signin-heading">Register</h2>
			<input id="email" name="email" type="email" class="form-control" placeholder="Email address" required autofocus>
			<input id="username" name="username" type="text" class="form-control" placeholder="Username" required>
			<input id="password" name="password" type="password" class="form-control" placeholder="Password" required>
			<button onclick="UserController.register();" class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
		  </div>

		</div> <!-- /container -->

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script src="ajax/UserController.js"></script>
		<script type="text/javascript">
			$(function(){

			})
		</script>
	</body>
</html>