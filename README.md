## **Deskripsi** ##

[Deskripsi Front-End]

## **Requirement** ##

1. Apache Web Server 2.4
1. PHP 5.4+
1. MySQL 5.0+

## **Instalasi** ##

1. Import database news_aggregator yang digunakan. (db/newsaggregator.sql)
1. Setting koneksi dengan database pada : php/Header.php.
1. Jalankan web pada server

## **Contributor** ##
[Nama]