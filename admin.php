<?php session_start();
    
    if(!isset($_SESSION["role"]) || $_SESSION["role"] != "admin") {
        echo "OKE";
        header('Location:  index.php');
        die();
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard - News Aggregator</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <link href="css/plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
        #dataTables-log {
            width: 100%;
            table-layout: fixed;
            word-wrap: break-word;
        }
    </style>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">News Aggregator</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                	<ul class="nav" id="side-menu">
                        <li>
                            <a href="admin.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Dashboard</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                	<div class="col-lg-8">
                		<div class="panel panel-default">
                			<div class="panel-heading">
	                            Crawler
	                        </div>
	                        <div class="panel-body">
	                        	
	                        	<div class="form-group col-sm-12">
	                        		<label class="col-sm-4">Crawler</label>
	                        		<div class="col-sm-3" id="status-crawler"></div>
	                        		<div class="col-sm-5">
	                        			<button id="start-button" type="button" onclick="Admin.start()" class="btn btn-primary">Start</button>
	                        			<button id="stop-button"  type="button" onclick="Admin.stop()" class="btn btn-danger">Stop</button>
	                        		     <span id="status-running"></span>
                                    </div>
	                        	</div>

	                        	<div class="form-group col-sm-12">
	                        		<label class="col-sm-4">
	                        			Durasi per RSS
	                        		</label>
	                        		<div class="input-group col-sm-5">
	                        			<input type="text" class="form-control" name="rss_time">
  										<span class="input-group-addon">Detik</span>
	                        		</div>
	                        	</div>

	                        	<!-- <div class="form-group col-sm-12">
	                        		<label class="col-sm-4">
	                        			Durasi per Site
	                        		</label>
	                        		<div class="input-group col-sm-5">
	                        			<input type="text" class="form-control" name="site_time">
  										<span class="input-group-addon">Detik</span>
	                        		</div>
	                        	</div> -->

	                        	<div class="form-group col-sm-12">
	                        		<label class="col-sm-4">
	                        			Durasi Cluster
	                        		</label>
	                        		<div class="input-group col-sm-5">
	                        			<input type="text" class="form-control" name="cluster_time">
  										<span class="input-group-addon">Detik</span>
	                        		</div>
	                        	</div>

                                <div class="form-group col-sm-12 ">
                                    <span id="status-config" class="text-left"></span>
                                    <button id="update-button" onclick="Admin.setConfig()" type="button" class="btn btn-primary pull-right">Update</button>
                                </div>

	                        </div>
                		</div>
                	</div>

                	<div class="col-lg-4">
                		<div class="panel panel-default">
                			<div class="panel-heading">
	                            Status
	                        </div>
	                        <div class="panel-body">
	                        	<div class="list-group">
                               	 	<div class="list-group-item col-sm-12">
                               	 		<div class="col-sm-6">
                                            <strong>Jumlah Artikel</strong>
                                        </div>
                                        <div class="col-sm-6 text-right" id="news_count">
                                            0
                                        </div>
                               	 	</div>
                                    <div class="list-group-item col-sm-12">
                                        <div class="col-sm-6">
                                            <strong>Jumlah Cluster</strong>
                                        </div>
                                        <div class="col-sm-6 text-right" id="clusters_count">
                                            0
                                        </div>
                                    </div>
                               	 </div>
	                        </div>
	                    </div>
	                </div>

	                <div class="col-lg-8">
                		<div class="panel panel-default">
                			<div class="panel-heading">
	                            Log
	                        </div>
	                        <div class="panel-body">
	                        	 <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-log">
                                    <thead>
                                        <tr>
                                            <th width="20%">Waktu</th>
                                            <th width="15%">Tag</th>
                                            <th>Aktivitas</th>
                                        </tr>
                                    </thead>
                                    <tbody id="content-table">
                                    </tbody>
                                </table>
	                        </div>
	                    </div>
	                </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/sb-admin-2.js"></script>


     <!-- DataTables JavaScript -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="js/fnFindCellRowIndexes.js"></script>
    <script type="text/javascript" src="js/moment.min.js"></script>
    <script src="ajax/Admin.js"></script>


    <script>
            $(function() {
                Admin.startLoad();
            });
        </script>

</body>

</html>
